# desafio-estagiario-1

### Desafio RPE de Seleção 
Olá, queremos convidá-lo a participar de nosso desafio de seleção.  Pronto para participar? Seu trabalho será visto por nosso time e você receberá ao final um feedback sobre o que achamos do seu trabalho. Não é legal?

### Sobre a oportunidade 
A vaga é para Estágio em Desenvolvimento Java.

### Desafio Técnico

  - Resumo do problema: Hoje na empresa estamos com muitos funcionários e também muitos clientes nos quais prestam serviços, e todo esse controle de cadastro desse pessoal está sendo através de papel físico. Com isso precisamos de uma sistema para manter o cadastro de todas essas pessoas (Funcionarios e clientes).
  
  
  - Dicionário:
    ```
    * Pessoa Funcionário: Funcionário da RPE que pode ser desenvolvedor, qa, gerente, RH, etc.
      Atributos: CPF, Nome, Endereco, Telefone, Função, Status, Data de Contratação
        
    * Pessoa Cliente: Cliente que presta serviço para RPE, exmplo eletricista, faxineiro, etc.
      Atributos: CPF, Nome, Endereco, Telefone, Data do último serviço
    ```

  Objetivo do Desafio: Desenvolver uma api que tenha uma função de CRUD para manter os cadastros das pessoas.    
  
    
  - Pré-requisitos:
    ```
    * Utilização de banco de dados Oracle, MySQL, H2, Postgres ou qualquer outro banco relacional.
    * Java 17+
    * Maven
    * Swagger

    ```

  - O que esperamos como escopo:
    ```
    * Endpoint para adicionar um Funcionário
    * Endpoint para adicionar um Cliente
    * Endpoint para consultar um Funcionário
    * Endpoint para consultar um Cliente
    * Endpoint para alterar um Funcionário
    * Endpoint para alterar um Cliente
    * Endpoint para remover um Funcionário
    * Endpoint para remover um Cliente

    ```

  - Não precisa desenvolver:
    ```
    * Front
    ```

  - Extra/Bônus (Não Obrigatório)
    ```
    * Implementar testes unitários
    ```
  
  - O que vamos avaliar:
    ```
    * Organização de código;
    * Funcionamento;
    * Boas práticas;
    ```

### Instruções
        1. Crie um projeto no gitlab ou github;
        2. Desenvolva. Você terá 5 (cinco) dias a partir da data do envio do desafio; 
        3. Crie um arquivo de texto com a nomenclatura README.md com a explicação de como devemos executar o 
        projeto e com uma descrição do que foi feito; 
        4. Envie o link do projeto respondendo ao email enviado.
